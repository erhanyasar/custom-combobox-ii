# Custom Combobox - II

Please visit [demo page](https://custom-combobox-ii.vercel.app/) to display this project without needing to do any of the steps below.

## Getting Started

- (Optional) First and foremost, download [`Node.js`](https://nodejs.org/en/download) if you haven't before running this application,
- After opening a terminal window on a directory of your preference, run `git clone https://gitlab.com/erhanyasar/custom-combobox-ii.git`,
- Run `cd custom-combobox-ii`,
- Run `pnpm i` or `yarn` or `npm i`,
- Run `pnpm dev` or `yarn dev` or `npm run dev`,

## Available Scripts

In the project directory, you can run:

### `pnpm start`

Runs the app in the development mode. Open [http://localhost:3000](http://localhost:3000) to view it in your browser. The page will reload when you make changes. You may also see any lint errors in the console.

### `pnpm test`

Launches the test runner in the interactive watch mode. See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `pnpm run build`

Builds the app for production to the `build` folder. It correctly bundles React in production mode and optimizes the build for the best performance.
The build is minified and the filenames include the hashes. Your app is ready to be deployed!
See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `pnpm run eject`

**Note: this is a one-way operation. Once you `eject`, you can't go back!**

If you aren't satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single-build dependency from your project.

## What's Next?

- Checkbox styles when a checkbox selected doesn't seem to its desired state as per the given brief,
- Deselection doesn't work as expected it should be putting back the deselected checkbox to the previous

# Troubleshooting

### `command not found: pnpm`

- Install [pnpm](https://pnpm.io/installation)

## Learn More

- This application initialized from [Vite boilerplate](https://vitejs.dev/guide/#trying-vite-online) with [TypeScript](https://www.typescriptlang.org/) template.
- To learn React, check out the [React documentation](https://reactjs.org/).
- To learn more about how this application deployed, check out [Vercel](https://vercel.com/) from [Next.js](https://nextjs.org/) creators.