import { Container, Box } from "@mui/material";
import { ComboboxWrapper } from "../components/comboboxWrapper/comboboxWrapper";
import "./App.css";

function App() {
  return (
    <Container className="app-container" maxWidth="sm">
      <Box className="app-box">
        <ComboboxWrapper />
      </Box>
    </Container>
  );
}

export default App;
