import React from 'react';
import { render } from "@testing-library/react";
import App from './App';

// import { toHaveStyle } from '@testing-library/jest-native/dist/to-have-style';
// expect.extend({ toHaveStyle });

describe("it should render App component", () => {
  const { baseElement} = render(<App />);

  test("renders App component", () => expect(baseElement).toBeTruthy());
  // test("includes max-width style", () => expect(baseElement.firstChild).toHaveStyle('max-width'));
  // test("includes app-box class", () => expect(baseElement.firstChild.firstChild).toHaveClass('app-box'));
})