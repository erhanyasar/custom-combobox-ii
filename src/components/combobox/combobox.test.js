import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import { Combobox } from './combobox';

describe("it should render Combobox component", () => {
  const { baseElement} = render(<Combobox />);
  test("renders Combobox component", () => expect(baseElement).toBeTruthy());
});

describe("it should succeed behavioural tests for Combobox", () => {
    const onSelectCategories = jest.fn();
    const onDeselectCategories = jest.fn();
    const mockSelectedCategories = ['Category 1', 'Category 2'];
    const mockFilteredCategories = ['Category 3', 'Category 4'];

    it('should render a FormGroup component with checkboxes for selected and filtered categories', () => {
        render(
        <Combobox
            onSelectCategories={onSelectCategories}
            onDeselectCategories={onDeselectCategories}
        />
        );

        expect(screen.getAllByRole('FormGroup')).toHaveLength(1);
    });

    it('should display selected categories with defaultChecked checkboxes and onDeselectCategories callback', () => {  
        jest.mock('../../store/context', () => ({
          useGlobalState: jest.fn(() => ({
            state: {
              selectedCategories: mockSelectedCategories,
              filteredCategories: mockFilteredCategories,
            },
          })),
        }));
  
        render(
          <Combobox
            onSelectCategories={onSelectCategories}
            onDeselectCategories={onDeselectCategories}
            filteredCategories={mockFilteredCategories}
            selectedCategories={mockSelectedCategories}
          />
        );
  
        const checkboxes = screen.getByRole('checkbox', { hidden: true });
        expect(checkboxes).toHaveLength(mockSelectedCategories.length);
        checkboxes.forEach((checkbox) => {
          expect(checkbox).toBeChecked();
          fireEvent.click(checkbox);
          expect(onDeselectCategories).toHaveBeenCalledTimes(1);
        });
    });

    it('should display filtered categories with unchecked checkboxes and onSelectCategories callback', () => {  
        const { getByRole } = render(
          <Combobox
            onSelectCategories={onSelectCategories}
            onDeselectCategories={onDeselectCategories}
            filteredCategories={mockFilteredCategories}
            selectedCategories={mockSelectedCategories}
          />
        );
  
        const checkboxes = getByRole('checkbox', { name: /checkbox/ }, { exact: false });
        expect(checkboxes).toHaveLength(mockFilteredCategories.length);
        checkboxes.forEach((checkbox) => {
          expect(checkbox).not.toBeChecked();
          fireEvent.click(checkbox);
          expect(onSelectCategories).toHaveBeenCalledTimes(1);
        });
      });

    it('should render no checkboxes when both selectedCategories and filteredCategories are empty arrays', () => {
      render(
        <Combobox
          onSelectCategories={onSelectCategories}
          onDeselectCategories={onDeselectCategories}
          filteredCategories={mockFilteredCategories}
          selectedCategories={mockSelectedCategories}
        />
      );
  
      expect(screen.queryAllByRole('checkbox')).toHaveLength(0);
    });

    it('should handle onDeselectCategories and onSelectCategories callbacks when selectedCategories and filteredCategories have only one category', () => {
      const onSelectCategories = jest.fn();
      const onDeselectCategories = jest.fn();
      const selectedCategories = ['Category 1'];
      const filteredCategories = ['Category 2'];
  
      render(
        <Combobox
          onSelectCategories={onSelectCategories}
          onDeselectCategories={onDeselectCategories}
          selectedCategories={mockSelectedCategories}
          filteredCategories={mockFilteredCategories}
        />
      );
  
      const checkboxes = screen.getAllByRole('checkbox');
      expect(checkboxes).toHaveLength(selectedCategories.length + filteredCategories.length);
      checkboxes.forEach((checkbox) => {
        if (checkbox.checked) {
          fireEvent.click(checkbox);
          expect(onDeselectCategories).toHaveBeenCalledTimes(1);
        } else {
          fireEvent.click(checkbox);
          expect(onSelectCategories).toHaveBeenCalledTimes(1);
        }
      });
    });

  it('should handle onDeselectCategories and onSelectCategories callbacks when selectedCategories and filteredCategories have multiple categories', () => {
    render(
      <Combobox
        onSelectCategories={onSelectCategories}
        onDeselectCategories={onDeselectCategories}
        selectedCategories={mockSelectedCategories}
        filteredCategories={mockFilteredCategories}
      />
    );
  
    const checkboxes = screen.getAllByRole('checkbox');
    expect(checkboxes).toHaveLength(mockSelectedCategories.length + mockFilteredCategories.length);
    checkboxes.forEach((checkbox) => {
      if (checkbox.checked) {
        fireEvent.click(checkbox);
        expect(onDeselectCategories).toHaveBeenCalledTimes(1);
      } else {
        fireEvent.click(checkbox);
        expect(onSelectCategories).toHaveBeenCalledTimes(1);
      }
    });
  });
});