import { Checkbox, FormGroup, FormControlLabel, Grid } from "@mui/material";
import { useGlobalState } from "../../store/context";
import { ComboboxProps } from "../../types";

export const Combobox: React.FC<ComboboxProps> = ({
  onSelectCategories,
  onDeselectCategories,
}: ComboboxProps) => {
  const { state } = useGlobalState();

  return (
    <Grid item xs={12} className="checkbox-group" my={2}>
      <FormGroup role="FormGroup">
        {state.selectedCategories.length !== 0 &&
          state.selectedCategories?.map((category: string, index: number) => {
            return (
              <FormControlLabel
                key={`${category}-${index}`}
                style={{ color: "#3064d0" }}
                control={
                  <Checkbox
                    role="checkbox"
                    name="checkbox"
                    aria-label={`${category} checkbox`}
                    defaultChecked
                    onChange={() => onDeselectCategories(index)}
                  />
                }
                label={category.replaceAll("&amp;", "&")}
              />
            );
          })}
        {state.filteredCategories.length !== 0 &&
          state.filteredCategories?.map((category: string, index: number) => {
            return (
              <FormControlLabel
                key={`s${category}-${index}`}
                control={
                  <Checkbox
                    checked={false}
                    role="checkbox"
                    name="checkbox"
                    aria-label={`${category} checkbox`}
                    onChange={() => onSelectCategories(index)}
                  />
                }
                label={category.replaceAll("&amp;", "&")}
              />
            );
          })}
      </FormGroup>
    </Grid>
  );
};
