import React from 'react';
import { render } from '@testing-library/react';
import { FilterInput } from './filterInput';

describe("it should render FilterInput component", () => {
    const { baseElement} = render(<FilterInput />);
    test("renders FilterInput component", () => expect(baseElement).toBeTruthy());
});