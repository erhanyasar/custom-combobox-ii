import { Grid, InputAdornment, OutlinedInput, Stack } from "@mui/material";
import { FilterInputProps } from "../../types";

export const FilterInput: React.FC<FilterInputProps> = ({ onInputChange }) => {
  return (
    <Grid item xs={12}>
      <Stack direction="column" sx={{ backgroundColor: "white" }}>
        <OutlinedInput
          aria-label="category search input"
          placeholder="kategori ara..."
          onChange={onInputChange}
          endAdornment={
            <InputAdornment position="end">
              <img className="search-img" src="search.svg" alt="search icon" />
            </InputAdornment>
          }
        />
      </Stack>
    </Grid>
  );
};
