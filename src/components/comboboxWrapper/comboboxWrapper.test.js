import React from 'react';
import { render } from '@testing-library/react';
import { ComboboxWrapper } from './ComboboxWrapper';

describe("it should render ComboboxWrapper component", () => {
    const { baseElement} = render(<ComboboxWrapper />);
    test("renders ComboboxWrapper component", () => expect(baseElement).toBeTruthy());
});