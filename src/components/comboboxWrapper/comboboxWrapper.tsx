import { useEffect, useState, ChangeEvent } from "react";
import { Button, Grid, Stack, Typography } from "@mui/material";
import { useGlobalState } from "../../store/context";
import { FilterInput } from "../filterInput/filterInput";
import { Combobox } from "../combobox/combobox";
import data from "../../assets/items.json";
// import { useSnackbar } from 'notistack';
import "./comboboxWrapper.css";

export const ComboboxWrapper = (): JSX.Element => {
  const { state, dispatch } = useGlobalState();

  const [searchInput, setSearchInput] = useState("");
  const [, setCategories] = useState<string[]>([]);
  const [, setSelectedCategories] = useState<string[]>([]);
  const [, setFilteredCategories] = useState<string[]>([]);

  useEffect(() => {
    /* It's way more proper to import json file like above but it's commented out
    * as below since it's explicitly told with the brief to use an http method

    const abortController = new AbortController();

    const fetchData = async () => {
      await fetch("../assets/items.json", {
        signal: abortController.signal,
      })
        .then(response => response.json())
        .then(result => setCategories([...result.data]))
        .catch(console.error);
    };

    try {
      fetchData();
        enqueueSnackbar("Data fetched successfully!", {variant: 'success'});
    } catch (err) {
      console.error(err);
      enqueueSnackbar("Error while fetching data!", {variant: 'error'});
    }

    return () => abortController.abort();
    */

    dispatch({ type: "saveCategories", payload: data.data });
  }, [dispatch]);

  useEffect(() => {
    setCategories([...state.categories]);
    setSelectedCategories([...state.selectedCategories]);
    setFilteredCategories([...state.filteredCategories]);
  }, [state.categories, state.filteredCategories, state.selectedCategories]);

  const handleInputChange = (e: ChangeEvent<HTMLInputElement>): void => setSearchInput(e.target.value);
  const handleSearchSubmit = (): void => dispatch({ type: "filterCategories", payload: searchInput });
  const handleSelectCategories = (i: number): void => dispatch({ type: "selectCategories", payload: i });
  const handleDeselectCategories = (i: number): void => dispatch({ type: "deselectCategories", payload: i });

  return (
    <Grid container className="combobox-wrapper" spacing={2}>
      <Grid item xs={12}>
        <Typography variant="h5" component="h1" gutterBottom className="combobox-header">
          Kategoriler
        </Typography>
      </Grid>

      <FilterInput onInputChange={handleInputChange} />

      <Combobox
        onSelectCategories={handleSelectCategories}
        onDeselectCategories={handleDeselectCategories}
      />

      <Grid item xs={12} my={1}>
        <Stack direction="column">
          <Button variant="contained" aria-label="search button" onClick={handleSearchSubmit}>
            Ara
          </Button>
        </Stack>
      </Grid>
    </Grid>
  );
};
