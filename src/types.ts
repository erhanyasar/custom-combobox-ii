import { Dispatch, ChangeEvent } from "react";

export type GlobalStateContextProps = {
  state: StateProps;
  dispatch: Dispatch<ActionProps>;
};

export type StateProps = {
  categories: string[];
  selectedCategories: string[];
  filteredCategories: string[];
};

export type ActionProps = {
  type: string;
  payload: unknown;
};

export type ComboboxProps = {
  onSelectCategories: (i: number) => void;
  onDeselectCategories: (i: number) => void;
};

export type FilterInputProps = {
  onInputChange: (e: ChangeEvent<HTMLInputElement>) => void;
};
