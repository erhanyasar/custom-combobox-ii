import React, { Suspense } from "react";
import ReactDOM from "react-dom/client";
import { CircularProgress, ThemeProvider, CssBaseline } from "@mui/material";
import { customTheme as theme } from "./utils/customTheme.ts";
import { GlobalStateProvider } from "./store/provider.tsx";
import { SnackbarProvider } from "notistack";
import App from "./app/App.tsx";
import "./index.css";

ReactDOM.createRoot(document.getElementById("root")!).render(
  <React.StrictMode>
    <Suspense fallback={<CircularProgress />}>
      <ThemeProvider theme={theme}>
        <GlobalStateProvider>
        <SnackbarProvider maxSnack={3}>
          <CssBaseline />
          <App />
          </SnackbarProvider>
        </GlobalStateProvider>
      </ThemeProvider>
    </Suspense>
  </React.StrictMode>
);
