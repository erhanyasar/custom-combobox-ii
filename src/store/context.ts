import { createContext, useContext } from "react";
import { initialState } from "../utils/initialState";
import { GlobalStateContextProps } from "../types";

export const GlobalStateContext = createContext<GlobalStateContextProps>({
  state: initialState,
  dispatch: () => {},
});

export const useGlobalState = () => useContext(GlobalStateContext);
