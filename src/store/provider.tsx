import { ReactNode, useReducer } from "react";
import { initialState } from "../utils/initialState";
import { GlobalStateReducer as reducer } from "./reducer";
import { GlobalStateContext } from "./context";

export const GlobalStateProvider = ({ children }: { children: ReactNode }) => {
  const [state, dispatch] = useReducer(reducer, initialState);

  return (
    <GlobalStateContext.Provider value={{ state, dispatch }}>
      {children}
    </GlobalStateContext.Provider>
  );
};
