import { initialState } from "../utils/initialState";
import { StateProps, ActionProps } from "../types";

export const GlobalStateReducer = (state: StateProps, action: ActionProps): StateProps => {
  const { type, payload } = action;

  let tempSelectedCategories: string[] = [],
    tempFilteredCategories: string[] = [];
  let indexOfDeselected = -1;

  switch (type) {
    case "saveCategories":
      return {
        ...state,
        categories: [...<[]>payload],
        filteredCategories: [...<[]>payload],
      };
    case "selectCategories":
      tempSelectedCategories = [...state.selectedCategories];
      tempSelectedCategories.push(state.filteredCategories[Number(payload)]);

      return {
        ...state,
        selectedCategories: [...tempSelectedCategories],
        filteredCategories: [
          ...state.filteredCategories.filter(
            (filteredCategory) => filteredCategory !== state.filteredCategories[<number>payload]
          ),
        ],
      };
    case "deselectCategories":
      state.categories.forEach((category, index) => {
        if (category === state.selectedCategories[<number>payload]) indexOfDeselected = index;
      });
      tempSelectedCategories = state.selectedCategories.filter(
        (category) => category !== state.selectedCategories[<number>payload]
      );

      tempFilteredCategories = [...state.filteredCategories];
      tempFilteredCategories.splice(indexOfDeselected, 0, state.selectedCategories[<number>payload]);
      return {
        ...state,
        selectedCategories: [...tempSelectedCategories],
        filteredCategories: [...tempFilteredCategories],
      };
    case "filterCategories":
      return {
        ...state,
        filteredCategories: [
          ...state.filteredCategories.filter((category) =>
            // It's more ideal to cover when casing doesn't match since it works as a fallback as per UX
            category.toLowerCase().includes(String(payload).toLowerCase())
          ),
        ],
      };
    case "reset":
      return initialState;
    default:
      console.warn(payload);
      return state;
  }
};
